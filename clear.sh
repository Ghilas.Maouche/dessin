#!/bin/bash

# Liste des fichiers et dossiers à conserver
files_to_keep=("CMakeCache.txt" "expressions" "parser" "run.sh" "clear.sh")

# Supprimer tous les fichiers et dossiers sauf ceux spécifiés
for file_or_dir in *; do
    if [[ ! " ${files_to_keep[@]} " =~ " ${file_or_dir} " ]]; then
        rm -r "${file_or_dir}"
        echo -e "Supprimé : ${file_or_dir}\n"
    fi
done

echo -e "[ Info ] Clean executed successfully\n"
