#!/bin/bash

# Liste des fichiers et dossiers à conserver
files_to_keep=("CMakeCache.txt" "expressions" "parser" "run.sh" "clear.sh")

# Supprimer tous les fichiers et dossiers sauf ceux spécifiés
for file_or_dir in *; do
    if [[ ! " ${files_to_keep[@]} " =~ " ${file_or_dir} " ]]; then
        rm -r "${file_or_dir}"
        echo "Supprimé : ${file_or_dir}"
    fi
done

echo "[ Info ] Clean executed successfully "

echo "[ Info ] start Cmake .. "
# build app
cmake ..

echo ""
echo "[ Info ] Cmake executed successfully.."
echo ""

echo "[ Info ] start make.."
make 
echo ""
echo "[ Info ] make executed successfully.."
echo ""

echo "[ Info ] start APP.."
# run app
./dessin 

